package ucsal.br.poo20182.bes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Agenda {
	public static void main(String[] args) {
		executar();
	}

	public static void executar() {

		// Variaveis
		ImageIcon icon = new ImageIcon("agendinha.png");
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		ArrayList<String> contatos = new ArrayList<String>();
		ArrayList<Contatos> contato = new ArrayList<Contatos>();

		int cont = 0;
		String[] menu = { "Incluir contato", "Excluir contato", "Listar contatos", "Pesquisar contato" };

		// Entrda
		JOptionPane.showMessageDialog(null, "Seja bem-vindo a sua agenda!", "Agenda", JOptionPane.INFORMATION_MESSAGE,
				icon);

		// Chama o metodo principal
		operacoes(date, menu, contatos, contato, cont, icon);
	}

	public static void operacoes(SimpleDateFormat date, String[] menu, ArrayList<String> contatos,
			ArrayList<Contatos> contato, int cont, ImageIcon icon) {

		// Processamento
		try {
			int respMenu;

			respMenu = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar", "Agenda",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, icon, menu, menu[0]);

			// De acordo com a resposta direciona para o metodo
			switch (respMenu) {
			case 0:
				contato.add(inserirContato(date, contatos, icon));
				break;
			case 1:
				int ct;
				ct = excluirContato(contatos, icon);
				contatos.remove(ct);
				contato.remove(ct);
				break;
			case 2:
				listarContatos(contatos, icon);
				break;
			case 3:
				int conta;
				conta = pesquisarContatos(contatos, icon);
				contatos.get(conta);
				mostrarContato(contato.get(conta), icon);
				break;
			}

			// metodo para reiniciar o codigo
			novoProcedimento(date, menu, contatos, contato, cont, icon);

		} catch (Exception e) {

			// Caso d� erro por classe estar vazia
			JOptionPane.showMessageDialog(null, "Ops! Contato n�o consta no servidor.", "Agenda",
					JOptionPane.INFORMATION_MESSAGE, icon);
			novoProcedimento(date, menu, contatos, contato, cont, icon);
		}
	}

	public static Contatos inserirContato(SimpleDateFormat date, ArrayList<String> contatos, ImageIcon icon)
			throws ParseException {
		// Variaveis
		String[] opcoes = { "Pessoal", "Profissional" };
		int tipo;
		Contatos contato = new Contatos();

		// Entrada
		contato.nome = JOptionPane.showInputDialog(icon, "Nome do contato:");
		contatos.add(contato.nome);
		contato.telefone = JOptionPane.showInputDialog(icon, "Telefone do contato:");
		contato.anoNascimento = Integer.parseInt(JOptionPane.showInputDialog(icon, "Ano do nascimento do contato:"));
		try {
			contato.dataNascimento = date.parse(JOptionPane.showInputDialog(icon, "Data de nascimento do contato:"));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "Ops! Digite no formato (XX/XX/XXXX)", "Agenda",
					JOptionPane.INFORMATION_MESSAGE, icon);
			contato.dataNascimento = date.parse(JOptionPane.showInputDialog(icon, "Data de nascimento do contato:"));
		}
		tipo = JOptionPane.showOptionDialog(null, "Tipo do contato: ", "Agenda", JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE, icon, opcoes, opcoes[0]);
		if (tipo == 0) {
			contato.tipo = Contatos.TipoContato.CONTATO_PESSOAL;
		}
		if (tipo == 1) {
			contato.tipo = Contatos.TipoContato.CONTATO_PROFISSIONAL;
		}

		return contato;
	}

	public static int excluirContato(ArrayList<String> contato, ImageIcon icon) {

		// Entrada
		String nome = JOptionPane.showInputDialog(icon, "Digite o nome do contato que deseja apagar:");

		// Processamento-Saida
		for (int i = 0; i < contato.size(); i++) {
			if (contato.get(i).equals(nome)) {
				JOptionPane.showMessageDialog(null, "Contato apagado", "Agenda", JOptionPane.INFORMATION_MESSAGE, icon);

				return i;
			} else if (i == contato.size()) {
				JOptionPane.showMessageDialog(null, "Contato n�o existe", "Agenda", JOptionPane.INFORMATION_MESSAGE,
						icon);
			}
		}

		return 9;
	}

	public static void listarContatos(ArrayList<String> contato, ImageIcon icon) {

		JOptionPane.showMessageDialog(null, contato.toArray(), "Contatos", JOptionPane.INFORMATION_MESSAGE, icon);

	}

	public static int pesquisarContatos(ArrayList<String> contato, ImageIcon icon) {

		// Entrada
		String nome = JOptionPane.showInputDialog(icon, "Digite o nome do contato que deseja pesquisar:");

		// Processamento-Saida
		for (int i = 0; i < contato.size(); i++) {
			if (contato.get(i).equals(nome)) {
				return i;
			} else if (i == contato.size()) {
				JOptionPane.showMessageDialog(null, "Contato n�o existe", "Agenda", JOptionPane.INFORMATION_MESSAGE,
						icon);
				break;
			}
		}

		return 9;
	}

	public static void mostrarContato(Contatos contato, ImageIcon icon) {

		JOptionPane.showMessageDialog(null, contato, "Contatos", JOptionPane.INFORMATION_MESSAGE, icon);

	}

	public static void novoProcedimento(SimpleDateFormat date, String[] menu, ArrayList<String> contatos,
			ArrayList<Contatos> contato, int cont, ImageIcon icon) {

		// Variaveis
		int respPerg;
		String[] sn = { "SIM", "N�O" };

		// Entrada
		respPerg = JOptionPane.showOptionDialog(null, "Deseja realizar outro procedimento?", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, icon, sn, 0);

		// Saida
		if (respPerg == 0) {
			operacoes(date, menu, contatos, contato, cont, icon);
		} else {
			JOptionPane.showMessageDialog(null, "At� a pr�xima!", "Agenda", JOptionPane.INFORMATION_MESSAGE, icon);
		}
	}
}
