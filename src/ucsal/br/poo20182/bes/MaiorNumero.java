package ucsal.br.poo20182.bes;

import java.util.Scanner;

public class MaiorNumero {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		executar(sc);
	}

	static void executar(Scanner sc) {

		// Variaveis
		int cont = 0;
		int ct = 1;
		int maior = 0;
		int[] numeros = new int[5];

		// Entrada
		obterNumeros(sc, cont, numeros);

		// Processamento
		maior = encontrarMaiorNumero(ct, maior, numeros);

		// Saida
		exibirMaiorNumero(maior);
	}

	static void exibirMaiorNumero(int maior) {
		System.out.println(maior);
	}

	static int encontrarMaiorNumero(int ct, int maior, int[] numeros) {
		int cont;
		cont = 0;
		while (ct < 5) {
			if (numeros[cont] > numeros[ct]) {
				if (numeros[cont] > maior) {
					maior = numeros[cont];
				}
			}
			cont++;
			ct++;
		}
		return maior;
	}

	static void obterNumeros(Scanner sc, int cont, int[] numeros) {
		System.out.println("Informe 5 numeros: (*apos cada um de enter)");
		while (cont < 5) {
			System.out.print("");
			numeros[cont] = sc.nextInt();
			cont++;
		}
	}

}
