package ucsal.br.poo20182.bes;

import org.junit.Assert;
import org.junit.Test;

public class MaiorNumeroTest {

	@Test
	public void teste01() {
		// Escolher dados a serem utilizados no teste
		int[] numeros = { 4, 8, 7, 4, 3 };
		int ct = 1;
		int maior = 0;

		// Executar o metodo que sera testado
		int result = MaiorNumero.encontrarMaiorNumero(ct, maior, numeros);

		// Definir o resultado esperado
		int resulEsperado = 8;

		// Comparar o resultado esperado com o obtido
		Assert.assertEquals(resulEsperado, result);

	}

}
