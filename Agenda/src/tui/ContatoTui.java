package tui;

import java.util.Date;

import javax.swing.JOptionPane;

import business.ContatoBO;
import persistence.ContatoDao;
import domain.Contato;
import enums.ContatoEnum;

public class ContatoTui {

	public static String incluir() {
		// Variaveis

		String[] opcoes = { "Pessoal", "Profissional" };
		ContatoEnum tipo = null;

		// Entrada
		String nome = JOptionPane.showInputDialog( "Nome do contato:", JOptionPane.OK_CANCEL_OPTION);
		if (nome == null){
			System.exit(0);
		}
		String telefone = JOptionPane.showInputDialog( "Telefone: (Formato: (XX) XXXXX-XXXX)", JOptionPane.OK_CANCEL_OPTION);
		if (telefone == null){
			System.exit(0);
		}
		telefone = ContatoBO.telefone(telefone);
		String ano = JOptionPane.showInputDialog( "Ano do nascimento:", JOptionPane.OK_CANCEL_OPTION);
		if (ano == null){
			System.exit(0);
		}
		int anoNascimento = Integer.parseInt(ContatoBO.ano(ano));
		String data = JOptionPane.showInputDialog( "Data de nascimento do contato:", JOptionPane.OK_CANCEL_OPTION);
		if (data == null){
			System.exit(0);
		}
		Date dataNascimento = ContatoBO.data(data);
		
		int tipoResp = JOptionPane.showOptionDialog(null, "Tipo do contato: ", "Agenda", JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);
		if (tipoResp == 0) {
			tipo = enums.ContatoEnum.PESSOAL;
		}
		if (tipoResp == 1) {
			tipo = enums.ContatoEnum.PROFISSIONAL;
		}

		Contato contato = new Contato(nome, telefone, anoNascimento, dataNascimento, tipo);
		ContatoDao.incluir(contato);

		return null;
	}

	public static void excluir() {

		// Entrada
		String nome = JOptionPane.showInputDialog( "Digite o nome do contato que deseja apagar:", JOptionPane.OK_CANCEL_OPTION);
		if (nome == null){
			System.exit(0);
		}
		ContatoDao.excluir(nome);

	}

	public static void listar() {
        ContatoDao.listar();

	}

	public static void pesquisar() {

		// Entrada
		String nome = JOptionPane.showInputDialog( "Digite o nome do contato que deseja pesquisar:", JOptionPane.OK_CANCEL_OPTION);
		if (nome == null){
			System.exit(0);
		}
		ContatoDao.pesquisar(nome);
		

		
	}

	

}
