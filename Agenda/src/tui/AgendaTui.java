package tui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class AgendaTui {

	public static void main(String[] args) {

		JOptionPane meuJOPane = new JOptionPane("      Seja bem-vindo a sua agenda!");
		final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
		dialogo.setModal(true);
		Timer timer = new Timer(1 * 1000, new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				dialogo.dispose();
			}
		});
		timer.start();
		dialogo.setVisible(true);
		timer.stop();

		operacoes();
	}

	public static void operacoes() {
		// Variaveis
		String[] menu = { "Incluir contato", "Excluir contato", "Listar contatos", "Pesquisar contato" };

		// Executar
		int respMenu = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar:", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, menu, menu[0]);
		switch (respMenu) {
		case 0:
			ContatoTui.incluir();
			novoProcedimento();
			break;
		case 1:
			ContatoTui.excluir();
			novoProcedimento();
			break;
		case 2:
			ContatoTui.listar();
			novoProcedimento();
			break;
		case 3:
			ContatoTui.pesquisar();
			novoProcedimento();
			break;

		}

	}

	public static void novoProcedimento() {
		// Variaveis
		int respPerg;
		String[] sn = { "SIM", "N�O" };

		// Entrada
		respPerg = JOptionPane.showOptionDialog(null, "Deseja realizar outro procedimento?", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, sn, sn[0]);

		// Saida
		if (respPerg == 0) {
			operacoes();
		} else {
			JOptionPane meuJOPane = new JOptionPane("At� a pr�xima!");
			final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
			dialogo.setModal(true);
			Timer timer = new Timer(2 * 1000, new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					dialogo.dispose();
				}
			});
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
		}
	}

}
