package domain;

import java.util.Date;

import enums.ContatoEnum;

public class Contato {

	private static String nome;
	private String telefone;
	private int anoNascimento;
	private Date dataNascimento;
	private ContatoEnum tipo;

	public String toString() {
		return "Nome: " + nome + "\nTelefone: " + telefone + "\nAno Nascimento: " + anoNascimento
				+ "\nData Nascimento: " + dataNascimento + "\n Tipo de Contato: " + tipo;
	}

	public Contato(String nome, String telefone, int anoNascimento, Date dataNascimento, ContatoEnum tipo) {
		Contato.nome = nome;
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
		this.dataNascimento = dataNascimento;
		this.tipo = tipo;
	}

	public static String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		Contato.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(int anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public ContatoEnum getTipo() {
		return tipo;
	}

	public void setTipo(ContatoEnum tipo) {
		this.tipo = tipo;
	}

}
