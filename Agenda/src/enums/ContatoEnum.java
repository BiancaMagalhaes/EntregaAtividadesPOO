package enums;

public enum ContatoEnum {
	PESSOAL, PROFISSIONAL;
	
	public static String obterItens(){
		String itens = "";
		   for(ContatoEnum tipo: values()){
			   itens += tipo + ",";
		   }
		   
		   return itens.substring(0, itens.length()-1);
		
	}

}
