package persistence;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import domain.Contato;

public class ContatoDao {
	public static List<Contato> contatos = new ArrayList<>();
	public static List<String> lista = new ArrayList<>();
	
    
	public static void incluir(Contato contato) {
		contatos.add(contato);
		lista.add(Contato.getNome());
	}

	public static void excluir(String nome) {
		if (contatos.size() == 0) {
			JOptionPane meuJOPane = new JOptionPane("Agenda Vazia!");
			final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
			dialogo.setModal(true);
			Timer timer = new Timer(2 * 1000, new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					dialogo.dispose();
				}
			});
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
		} else {
			for (int i = 0; i < contatos.size(); i++) {
				contatos.get(i);
				if (Contato.getNome().equals(nome)) {
					contatos.remove(i);
					JOptionPane meuJOPane = new JOptionPane("Contato apagado");
					final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
					dialogo.setModal(true);
					Timer timer = new Timer(2 * 1000, new ActionListener() {
						public void actionPerformed(ActionEvent ev) {
							dialogo.dispose();
						}
					});
					timer.start();
					dialogo.setVisible(true);
					timer.stop();

				} else if (i == contatos.size() - 1) {
					JOptionPane meuJOPane = new JOptionPane("Contato n�o existe");
					final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
					dialogo.setModal(true);
					Timer timer = new Timer(2 * 1000, new ActionListener() {
						public void actionPerformed(ActionEvent ev) {
							dialogo.dispose();
						}
					});
					timer.start();
					dialogo.setVisible(true);
					timer.stop();
					
				}
			}
		}
	}

	public static void pesquisar(String nome) { 
		if (contatos.size() == 0) {
			JOptionPane meuJOPane = new JOptionPane("Agenda Vazia");
			final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
			dialogo.setModal(true);
			Timer timer = new Timer(2 * 1000, new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					dialogo.dispose();
				}
			});
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
		} else {
			for (int i = 0; i < contatos.size(); i++) {
				contatos.get(i);
				if (Contato.getNome().equals(nome)) {
					mostrar(contatos.get(i));
				} else if (i == contatos.size() - 1) {
					JOptionPane meuJOPane = new JOptionPane("Contato n�o existe");
					final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
					dialogo.setModal(true);
					Timer timer = new Timer(1 * 1000, new ActionListener() {
						public void actionPerformed(ActionEvent ev) {
							dialogo.dispose();
						}
					});
					timer.start();
					dialogo.setVisible(true);
					timer.stop();
					break;
				}
			}
		}
	
	}

	public static void mostrar(Contato contato) {

		JOptionPane.showMessageDialog(null,Contato.getNome() + "\n" + contato.getTelefone() + "\n" + contato.getDataNascimento() + "\n"
						+ contato.getAnoNascimento() + "\n" + contato.getTipo(),
				"Contatos", JOptionPane.INFORMATION_MESSAGE);

	}

	public static void listar() {
		JOptionPane.showMessageDialog(null, lista.toArray(), "Contatos", JOptionPane.INFORMATION_MESSAGE);
		
	}

}
