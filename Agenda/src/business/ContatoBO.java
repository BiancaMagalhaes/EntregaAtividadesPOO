package business;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class ContatoBO {
	
	static JOptionPane meuJOPane = new JOptionPane();
	final static JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
	static Timer timer = new Timer(2 * 1000, new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
			dialogo.dispose();
		}
	});
	
	public static String telefone(String telefone) {
		if(telefone.length() != 15) {
			meuJOPane = new JOptionPane("Telefone inserido em formato diferente do suportado");
			dialogo.setModal(true);
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
			telefone = JOptionPane.showInputDialog("Telefone: (Formato: (XX) XXXXX-XXXX)", JOptionPane.OK_CANCEL_OPTION);
			telefone(telefone);
		}
		return telefone;
	}

	public static String ano(String ano) {
		meuJOPane = new JOptionPane("Ano inv�lido");
		if(ano.length() != 4) {
			
			dialogo.setModal(true);
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
			ano = JOptionPane.showInputDialog("Ano do nascimento:", JOptionPane.OK_CANCEL_OPTION);
		}else if ((!ano.substring(0, 1).equals("1")) && (!ano.substring(0, 1).equals("2"))){
			JOptionPane meuJOPane = new JOptionPane("Ano inv�lido");
			final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
			dialogo.setModal(true);
			Timer timer = new Timer(2 * 1000, new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					dialogo.dispose();
				}
			});
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
			ano = JOptionPane.showInputDialog("Ano do nascimento:", JOptionPane.OK_CANCEL_OPTION);
			ano(ano);
		}else if ((!ano.substring(1, 2).equals("0")) && (!ano.substring(1, 2).equals("9"))){
			JOptionPane meuJOPane = new JOptionPane("Ano inv�lido");
			final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
			dialogo.setModal(true);
			Timer timer = new Timer(2 * 1000, new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					dialogo.dispose();
				}
			});
			timer.start();
			dialogo.setVisible(true);
			timer.stop();
			ano = JOptionPane.showInputDialog("Ano do nascimento:", JOptionPane.OK_CANCEL_OPTION);
			ano(ano);
		}
		return ano;
		
	}

	public static Date data(String data) {
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		Date dataNascimento = null;
		try {
		dataNascimento = date.parse(data);
	} catch (ParseException e) {
		JOptionPane meuJOPane = new JOptionPane("Ops! Digite no formato (XX/XX/XXXX)");
		final JDialog dialogo = meuJOPane.createDialog(null, "Agenda");
		dialogo.setModal(true);
		Timer timer = new Timer(2 * 1000, new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				dialogo.dispose();
			}
		});
		timer.start();
		dialogo.setVisible(true);
		timer.stop();
		data = JOptionPane.showInputDialog("Data de nascimento do contato:", JOptionPane.OK_CANCEL_OPTION);
		data(data);
	}
		return dataNascimento;
	}

}
