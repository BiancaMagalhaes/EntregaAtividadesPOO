package persistence;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import domain.Contato;

public class ContatoDao {
	public List<Contato> contatos = new ArrayList<>();
	public static ImageIcon icon = new ImageIcon("agendinha.png");

	public String incluir(Contato contato) {
		contatos.add(contato);
		return null;
	}
	
	public String excluir (String nome) {
		contatos.remove(nome);
		for (int i = 0; i < contatos.size(); i++) {
			if (contatos.get(i).equals(nome)) {
				JOptionPane.showMessageDialog(null, "Contato apagado", "Agenda", JOptionPane.INFORMATION_MESSAGE, icon);

				return "0";
			} else if (i == contatos.size()) {
				JOptionPane.showMessageDialog(null, "Contato n�o existe", "Agenda", JOptionPane.INFORMATION_MESSAGE,
						icon);
			}
		}
		return null;
	}

	public List<Contato> obterTodos() {
		return contatos;
	}

}
