package tui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import persistence.ContatoDao;
import domain.Contato;
import domain.Contato.TipoContato;

public class ContatoTui {
	
	
	public static ImageIcon icon = new ImageIcon("agendinha.png");
	public static SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
	
	public static String incluir(){
		// Variaveis
		
		String[] opcoes = { "Pessoal", "Profissional" };
		TipoContato tipo = null;
		
		

		// Entrada
		String nome = JOptionPane.showInputDialog(icon, "Nome do contato:");
		String telefone = JOptionPane.showInputDialog(icon, "Telefone do contato:");
		int anoNascimento = Integer.parseInt(JOptionPane.showInputDialog(icon, "Ano do nascimento do contato:"));
		Date dataNascimento = null;
		try {
			dataNascimento = date.parse(JOptionPane.showInputDialog(icon, "Data de nascimento do contato:"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int tipoResp = JOptionPane.showOptionDialog(null, "Tipo do contato: ", "Agenda", JOptionPane.YES_NO_OPTION,
				JOptionPane.INFORMATION_MESSAGE, icon, opcoes, opcoes[0]);
		if (tipoResp == 0) {
			tipo = domain.Contato.TipoContato.CONTATO_PESSOAL;
		}
		if (tipoResp == 1) {
			tipo = domain.Contato.TipoContato.CONTATO_PROFISSIONAL;
		}
		
		Contato contato = new Contato(nome, telefone, anoNascimento, dataNascimento, tipo);

		return null;
	}
	
	public static String excluir() {

		// Entrada
		String nome = JOptionPane.showInputDialog(icon, "Digite o nome do contato que deseja apagar:");

		return null;
	}
	
	public static void listar() {

		JOptionPane.showMessageDialog(null, Contato.getNome().toCharArray(), "Contatos", JOptionPane.INFORMATION_MESSAGE, icon);

	}

	public static int pesquisar() {

		// Entrada
		String nome = JOptionPane.showInputDialog(icon, "Digite o nome do contato que deseja pesquisar:");

		// Processamento-Saida
		for (int i = 0; i < Contato.length; i++) {
			if (Contato.getNome().equals(nome)) {
				return i;
			} else if (i == Contato.size()) {
				JOptionPane.showMessageDialog(null, "Contato n�o existe", "Agenda", JOptionPane.INFORMATION_MESSAGE,
						icon);
				break;
			}
		}

		return 9;
	}

	public static void mostrar(Contato contato) {

		JOptionPane.showMessageDialog(null, contato, "Contatos", JOptionPane.INFORMATION_MESSAGE, icon);

	}


	
}
