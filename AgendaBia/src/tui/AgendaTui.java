package tui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import domain.Contato;


public class AgendaTui {
	
	public static ImageIcon icon = new ImageIcon("agendinha.png");
	public static SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
	
	public void main(String [] args){
		
		//Executar
		JOptionPane.showMessageDialog(null, "Seja bem-vindo a sua agenda!", "Agenda", JOptionPane.INFORMATION_MESSAGE,
				icon);
		
	}
	
	public static void operacoes (){
		//Variaveis
				String[] menu = { "Incluir contato", "Excluir contato", "Listar contatos", "Pesquisar contato" };
		
		//Executar
				
		int respMenu = JOptionPane.showOptionDialog(null, "Escolha a a��o que deseja realizar", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, icon, menu, menu[0]);
		switch (respMenu) {
		case 0:
			ContatoTui.incluir();
			break;
		case 1:
			ContatoTui.excluir();
			break;
		case 2:
			ContatoTui.listar();
			break;
		/*case 3:
			ContatoTui.pesquisar();
			int conta;
			conta = pesquisarContatos(contatos, icon);
			contatos.get(conta);
			mostrarContato(contato.get(conta), icon);
			break;*/
		}
	
	}
	
	public static void novoProcedimento() {
		// Variaveis
		int respPerg;
		String[] sn = { "SIM", "N�O" };

		// Entrada
		respPerg = JOptionPane.showOptionDialog(null, "Deseja realizar outro procedimento?", "Agenda",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, icon, sn, 0);

		// Saida
		if (respPerg == 0) {
			operacoes();
		} else {
			JOptionPane.showMessageDialog(null, "At� a pr�xima!", "Agenda", JOptionPane.INFORMATION_MESSAGE, icon);
		}
	}

}
