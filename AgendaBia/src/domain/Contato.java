package domain;

import java.util.Date;

public class Contato {

	private static String nome;
	private String telefone;
	private int anoNascimento;
	private Date dataNascimento;

	public enum TipoContato {
		CONTATO_PROFISSIONAL, CONTATO_PESSOAL
	}

	private TipoContato tipo;

	public String toString() {
		return "Nome: " + nome + "\nTelefone: " + telefone + "\nAno Nascimento: " + anoNascimento
				+ "\nData Nascimento: " + dataNascimento + "\n Tipo de Contato: " + tipo;
	}

	public Contato(String nome, String telefone, Integer anoNascimento, Date dataNascimento, TipoContato tipo) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
		this.dataNascimento = dataNascimento;
		this.tipo = tipo;
	}

	public static String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(int anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public TipoContato getTipo() {
		return tipo;
	}

	public void setTipo(TipoContato tipo) {
		this.tipo = tipo;
	}

}
